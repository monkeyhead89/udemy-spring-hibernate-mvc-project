package com.mseymour.springdemo.service.interfaces;

import com.mseymour.springdemo.entity.customer.Customer;

import java.util.List;

/**
 * Created by mattseymour on 06/03/2017.
 */
public interface CustomerService {

    public List<Customer> getCustomers();

    public void saveCustomer(Customer customer);

    public Customer getCustomer(int customerId);

    public void deleteCustomer(int customerId);

    public List<Customer> searchCustomers(String theSearchName);

}
