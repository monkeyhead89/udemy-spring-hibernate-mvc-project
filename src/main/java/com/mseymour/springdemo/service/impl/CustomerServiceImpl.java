package com.mseymour.springdemo.service.impl;

import com.mseymour.springdemo.dao.interfaces.CustomerDAO;
import com.mseymour.springdemo.entity.customer.Customer;
import com.mseymour.springdemo.service.interfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by mattseymour on 06/03/2017.
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    //Need to inject customerDAO
    @Autowired
    private CustomerDAO customerDAO;

    @Override
    @Transactional
    public List<Customer> getCustomers() {
        return customerDAO.getCustomers();
    }

    @Override
    @Transactional
    public void saveCustomer(Customer customer) {
        customerDAO.saveCustomer(customer);
    }

    @Override
    @Transactional
    public Customer getCustomer(int customerId) {
        Customer customer = customerDAO.getCustomer(customerId);
        return customer;
    }

    @Override
    @Transactional
    public void deleteCustomer(int customerId) {
        customerDAO.deleteCustomer(customerId);
    }


    @Override
    @Transactional
    public List<Customer> searchCustomers(String theSearchName) {
        return customerDAO.searchCustomers(theSearchName);
    }
}
