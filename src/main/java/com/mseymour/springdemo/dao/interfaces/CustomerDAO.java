package com.mseymour.springdemo.dao.interfaces;

import com.mseymour.springdemo.entity.customer.Customer;

import java.util.List;

/**
 * Created by mattseymour on 04/03/2017.
 */
public interface CustomerDAO {

    public List<Customer> getCustomers();

    public void saveCustomer(Customer customer);

    public Customer getCustomer(int customerId);

    public void deleteCustomer(int customerId);

    List<Customer> searchCustomers(String customerName);
}
