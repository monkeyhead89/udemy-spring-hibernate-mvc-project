package com.mseymour.springdemo.dao.impl;

import com.mseymour.springdemo.dao.interfaces.CustomerDAO;
import com.mseymour.springdemo.entity.customer.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by mattseymour on 04/03/2017.
 */
@Repository
public class CustomerDAOImpl implements CustomerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Customer> getCustomers() {

        //Get current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();

        // Create query
        Query<Customer> getCustomersQuery = currentSession.createQuery("from Customer order by lastName",Customer.class);

        // Execute query and get the result
        List<Customer> customers = getCustomersQuery.getResultList();

        // Return results
        return customers;
    }

    @Override
    public void saveCustomer(Customer customer) {

        // Get current session
        Session currentSession = sessionFactory.getCurrentSession();

        // Save the customer either by creating or updating existing record to database
        currentSession.saveOrUpdate(customer);
    }

    @Override
    public Customer getCustomer(int customerId) {

        // Get current session
        Session currentSession = sessionFactory.getCurrentSession();

        // Get required customer object from db
        Customer customer = currentSession.get(Customer.class,customerId);

        // Return customer object
        return customer;
    }

    @Override
    public void deleteCustomer(int customerId) {

        // Get current session
        Session currentSession = sessionFactory.getCurrentSession();

        // Create delete query to delete required customer
        Query deleteCustomerQuery =  currentSession.createQuery("delete from Customer where id=:customerId");

        // Set query param
        deleteCustomerQuery.setParameter("customerId",customerId);

        // Execute query
        deleteCustomerQuery.executeUpdate();
    }

    @Override
    public List<Customer> searchCustomers(String customerName) {
        // get the current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();

        Query theQuery = null;

        //
        // only search by name if theSearchName is not empty
        //
        if (customerName != null && customerName.trim().length() > 0) {

            // search for firstName or lastName ... case insensitive
            theQuery =currentSession.createQuery("from Customer where lower(firstName) like :theName or lower(lastName) like :theName", Customer.class);
            theQuery.setParameter("theName", "%" + customerName.toLowerCase() + "%");

        }
        else {
            // theSearchName is empty ... so just get all customers
            theQuery =currentSession.createQuery("from Customer", Customer.class);
        }

        // execute query and get result list
        List<Customer> customers = theQuery.getResultList();

        // return the results
        return customers;

    }
}
