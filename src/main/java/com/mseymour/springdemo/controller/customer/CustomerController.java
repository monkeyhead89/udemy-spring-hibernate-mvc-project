package com.mseymour.springdemo.controller.customer;

import com.mseymour.springdemo.entity.customer.Customer;
import com.mseymour.springdemo.form.SearchForm;
import com.mseymour.springdemo.service.interfaces.CustomerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    // Add log4j logger
    static Logger log = Logger.getLogger(CustomerController.class.getName());

    // Service dependency injection
    @Autowired
    private CustomerService customerService;

    @GetMapping("/list")
    public String listCustomers(@ModelAttribute SearchForm searchForm,Model customerModel){

        log.info("Listing those customers!");

        // Get all the customers
        List<Customer> customers = customerService.getCustomers();

        // Add the customer object to the model
        customerModel.addAttribute("customers",customers);

        // Set the page title dynamically
        customerModel.addAttribute("title","List All Customers");

        // Return the customerList view
        return "customer/list";
    }

    @RequestMapping("/add")
    public String addNewCustomer(@ModelAttribute Customer customer,  Model customerModel){

        // Set the page title dynamically
        customerModel.addAttribute("title","Add A New Customer");

        // Return the customer/add view
        return "customer/add";
    }

    @PostMapping("/save")
    public String saveCustomer(@Valid @ModelAttribute Customer customer, BindingResult errors){

        // Return to the form if it has errors
        if(errors.hasErrors()){
            if(customer.getId() > 0){
                return "customer/edit";
            }else{
                return "customer/add";
            }
        }

        // Delegate to customer service to save customer to db
        customerService.saveCustomer(customer);

        // Redirect to the customer list view
        return "redirect:/customer/list";
    }

    @GetMapping("/edit")
    public String editCustomer(@RequestParam("customerId") int customerId,
                                    Model customerModel
                                    ){

        // Get customer from the service
        Customer customer = customerService.getCustomer(customerId);

        // Set the customer object as model attribute so we can populate form
        customerModel.addAttribute("customer",customer);

        // Set the page title dynamically
        customerModel.addAttribute("title","Edit Customer");

        // Return to the edit customer form view
        return "customer/edit";
    }

    @GetMapping("/delete")
    public String deleteCustomer(@RequestParam("customerId") int customerId){

        // Delete customer using service
       customerService.deleteCustomer(customerId);

        // Return to list customer view
        return "redirect:/customer/list";
    }

    @PostMapping("/search")
    public String searchCustomers(@ModelAttribute SearchForm searchForm,
                                  Model theModel) {

        // search customers from the service
        List<Customer> customers = customerService.searchCustomers(searchForm.getSearchName()   );

        // add the customers to the model
        theModel.addAttribute("customers", customers);

        return "customer/list";
    }
}
