package com.mseymour.springdemo.controller.index;

import com.mseymour.springdemo.entity.customer.Customer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by mattseymour on 05/03/2017.
 */
@Controller
@RequestMapping("/")
public class IndexController {

    @GetMapping("/")
    public String index(Model model){

        // Set the page title
        model.addAttribute("title","Welcome!");

        return "index/index";
    }
}
