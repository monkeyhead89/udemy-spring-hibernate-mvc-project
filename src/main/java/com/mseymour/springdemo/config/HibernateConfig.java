package com.mseymour.springdemo.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.beans.PropertyVetoException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySources({
        @PropertySource("classpath:hibernate.properties"),
        @PropertySource("classpath:db.properties")
})
public class HibernateConfig {

    @Autowired
    Environment environment;

    @Bean
    public ComboPooledDataSource getDataSource(){

        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        try {
            dataSource.setDriverClass(environment.getProperty("jdbc.driver"));
            dataSource.setJdbcUrl(environment.getProperty("jdbc.url"));
            dataSource.setUser(environment.getProperty("jdbc.username"));
            dataSource.setPassword(environment.getProperty("jdbc.password"));
            dataSource.setMinPoolSize(Integer.valueOf(environment.getProperty("jdbc.minConnectionPoolSize")));
            dataSource.setMaxPoolSize(Integer.valueOf(environment.getProperty("jdbc.maxConnectionPoolSize")));
            dataSource.setMaxIdleTime(Integer.valueOf(environment.getProperty("jdbc.maxConnectionIdleTime")));
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }

        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean getSessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(getDataSource());
        sessionFactory.setPackagesToScan(environment.getProperty("entities.package"));

        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect",environment.getProperty("hibernate.dialect"));
        hibernateProperties.setProperty("hibernate.show_sql",environment.getProperty("hibernate.show_sql"));

        sessionFactory.setHibernateProperties(hibernateProperties);
        return sessionFactory;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }
}
