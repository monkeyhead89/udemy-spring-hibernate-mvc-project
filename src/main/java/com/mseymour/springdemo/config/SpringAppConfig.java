package com.mseymour.springdemo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by mattseymour on 15/03/2017.
 */

@Configuration
@ComponentScan({ "com.mseymour.springdemo" })
@EnableAspectJAutoProxy
public class SpringAppConfig {
}
