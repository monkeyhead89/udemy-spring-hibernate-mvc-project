package com.mseymour.springdemo.servlet;

import com.mseymour.springdemo.config.SpringAppConfig;
import com.mseymour.springdemo.config.SpringServletConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Created by mattseymour on 15/03/2017.
 */
public class InitialServlet extends AbstractAnnotationConfigDispatcherServletInitializer {

        @Override
        protected Class<?>[] getRootConfigClasses() {
            return new Class[] { SpringAppConfig.class };
        }

        @Override
        protected Class<?>[] getServletConfigClasses() {
            return new Class[] { SpringServletConfig.class };
        }

        @Override
        protected String[] getServletMappings() {
            return new String[] { "/" };
        }


}
