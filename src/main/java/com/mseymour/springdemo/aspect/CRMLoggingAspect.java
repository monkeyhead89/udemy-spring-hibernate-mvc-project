package com.mseymour.springdemo.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.logging.Logger;

@Aspect
@Component
public class CRMLoggingAspect {

    // Set up logger
    private Logger logger = Logger.getLogger(getClass().getName());

    // Set up pointcut declerations
    @Pointcut("execution(* com.mseymour.springdemo.controller.*.*.*(..))")
    private void forControllerPackage() {

    }

    @Pointcut("execution(* com.mseymour.springdemo.service.*.*.*(..))")
    private void forServicePackage() {

    }

    @Pointcut("execution(* com.mseymour.springdemo.dao.*.*.*(..))")
    private void forDaoPackage() {

    }

    @Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
    private void forAppFlow() {
    }

    // Add @before advice
    @Before("forAppFlow()")
    public void before(JoinPoint joinPoint) {

        // Display method we are calling
        logger.info("=======> in @Before : calling method " + joinPoint.getSignature().toShortString());

        // Display method params
        Object[] args = joinPoint.getArgs();

        Arrays.stream(args)
                .forEach(x -> logger.info("======> " + x));

    }

    // Add @afterReturning advice
    @AfterReturning(pointcut = "forAppFlow()",
            returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {

        // Display method we are calling
        logger.info("=======> in @AfterReturning : calling method " + joinPoint.getSignature().toShortString());

        //Display data returned
        logger.info("=======> result " + result);
    }
}
